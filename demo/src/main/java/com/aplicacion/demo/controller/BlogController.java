package com.aplicacion.demo.controller;


import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aplicacion.demo.model.Blog;
import com.aplicacion.demo.service.BlogService;

@RestController
public class BlogController {
	
	@Autowired
	private BlogService blogService;
	
	
	// search data with parameters
    @RequestMapping(value = "/blog/search/{searchType}", method = RequestMethod.POST)
    public ResponseEntity<List<Blog>> searchToDoBlog(@PathVariable("searchType") byte searchType, @RequestBody Blog blog) {
    	
		return new ResponseEntity<List<Blog>>(blogService.getBlog(searchType, 
																	blog.getId(),
																	blog.getTitulo(), 
																	blog.getContenido()), 
																	HttpStatus.OK);
		
   	}
    
    
    @RequestMapping(value = "/blog/create", method = RequestMethod.POST)
   	public ResponseEntity<Blog> saveToDo(@RequestBody Blog blog) {
    	
    	byte searchType = 4;
    	List<Blog> blogResult = blogService.getBlog(searchType,blog.getId(), "", "");
    	blog.setId(blogResult.get(0).getId() + 1);
    	blog.setTitulo("Titulo " +blog.getId() +" Otro");
    	blog.setContenido("Contenido " +blog.getId() +" Otro");
		return new ResponseEntity<Blog>(blogService.saveBlog(blog), 
										HttpStatus.OK);
   	}
    
    @RequestMapping(value = "/blog/update", method = RequestMethod.PUT)
   	public ResponseEntity<Blog>  updateToDo(@RequestBody Blog blog) {
		return new ResponseEntity<Blog>(blogService.saveBlog(blog), 
										HttpStatus.OK);
   	}
    
    @RequestMapping(value = "/blog/{id}/delete", method = RequestMethod.DELETE)
	public ResponseEntity<String> removeToDoById(@PathVariable("id") long id){
    	byte searchType = 2;
    	List<Blog> blog = blogService.getBlog(searchType, id, "", "");
    	blogService.removeBlog(blog.get(0));
		return new ResponseEntity<String>("Blog eliminado", 
											HttpStatus.OK);
	}
}

