package com.aplicacion.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.aplicacion.demo.model.Blog;

public interface BlogRepository extends JpaRepository<Blog, Long>{
	
	List<Blog> findByTituloContaining(@Param("titulo") String titulo);
	
	List<Blog> findById(@Param("id") long id);
	List<Blog> findAll();
	List<Blog> findTop1ByOrderByIdDesc();// obtengo el ultimo id de la BD
	List<Blog>findByTituloContainingOrContenidoContaining(@Param("titulo") String titulo,
														  @Param("contenido") String contenido);
	
	

}
