package com.aplicacion.demo.service;

import java.util.List;

import javax.validation.constraints.Null;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aplicacion.demo.model.Blog;
import com.aplicacion.demo.repository.BlogRepository;

@Service
public class BlogService {
	
	@Autowired
	private BlogRepository blogRepository;
	
	
	public List<Blog> getBlog(byte searchType, @Null Long kId,@Null String kTitle, @Null String kContent){
		
		
		switch(searchType){
			case 1:{
				// get all
				return blogRepository.findAll();
			}
			case 2:{
				if(kId == null)
					break;
				// get by id
				return blogRepository.findById(kId);
			}
			case 3:{
				if(kTitle == null)
					break;
				// get by Title
				return blogRepository.findByTituloContaining(kTitle);
			}
			case 4:{
				// get last id
				return blogRepository.findTop1ByOrderByIdDesc();
			}
			case 5:{
				// get  - containing term
				return blogRepository.findByTituloContainingOrContenidoContaining(kTitle,kContent);
			}
		}
		
		return null;
		
	}
	

	public Blog saveBlog(Blog blog){
		return blogRepository.save(blog);
	}
	
	public void removeBlog(Blog blog){
		blogRepository.delete(blog);
	}
	
	
	
	
	
	
	
	
	
	/*public List<Blog> getAllBlog(){
		return blogRepository.findAll();
	}
	
	public Blog getBlogById(long id){
		//return blogRepository.findOne(id);
		return blogRepository.findById(id);
	}
	
	public Blog getBlogByTitle(String titulo){
		//return blogRepository.findOne(id);
		return blogRepository.findByTitulo(titulo);
	}
	
	public List<Blog> getBlogByContentAndTitle(String contenido, String titulo) {
		
		return this.blogRepository.findByContenidoAndTitulo(contenido, titulo);
	}
	*/
	
	
}

